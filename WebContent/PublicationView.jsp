<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Publication</title>
</head>
<body>
	<table>
		<tr>
		<th>Id</th><th>Title</th><th>Publication Name</th><th>Publication Date</th><th>Cite Count</th><th>Authors</th>
		</tr>
		<tr>
			<td>${pi.id}</td>
	        <td>${pi.title}</td>
	        <td>${pi.publicationName}</td>
	        <td>${pi.publicationDate}</td>
	        <td>${pi.citeCount}</td>        
	        <td>${pi.authors}</td>
		</tr>
	</table>
	<form action="UpdateCitationsAPIServlet?id=${pi.id}" method="post">
		<button type="submit"><td>Update Citation</td>	</button>
	</form>
</body>
</html>