package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;


/**
 * Servlet implementation class CreatePublicationServlet
 */
@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Publication p = new Publication();
		p.setId((String) request.getParameter("id"));
		p.setAuthors((String) request.getParameter("authors"));
		p.setCiteCount(Integer.parseInt(request.getParameter("citeCount")));
		p.setPublicationDate((String) request.getParameter("publicationDate"));
		p.setPublicationName((String) request.getParameter("publicationName"));
		p.setTitle((String) request.getParameter("title"));
		
		
		Client client = ClientBuilder.newClient(new ClientConfig());
		client.target("http://localhost:8080/CRISSERVICE/rest/Publications/"
				).request().post(Entity.entity(p, MediaType.APPLICATION_JSON));
		response.sendRedirect(request.getContextPath() + "/AdminServlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
